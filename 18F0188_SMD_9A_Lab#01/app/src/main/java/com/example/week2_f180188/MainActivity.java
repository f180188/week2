package com.example.week2_f180188;
import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText username = findViewById(R.id.username);
        EditText password = findViewById(R.id.password);

        Button loginButton = findViewById(R.id.button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (username.getText().toString().equals("f180188") && password.getText().toString().equals("12345") ) {
                    Toast.makeText(MainActivity.this, "Successfully Logged In ! Hurray..", Toast.LENGTH_SHORT).show();
                }
                else {
                    DialogueBox Dialog = new DialogueBox();
                    Dialog.show(getSupportFragmentManager(), "Example");
                }
            }
        });
    }

}