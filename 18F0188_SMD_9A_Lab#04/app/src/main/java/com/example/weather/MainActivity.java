package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.weather.R;

import java.text.DecimalFormat;


public class MainActivity extends AppCompatActivity {
    public static final String message_key = "email";
    EditText City;
    TextView Result;
    private final String url = "http://api.weatherstack.com/current";
    private final String key = "ff0fd078fd5ffa42ee7c523a71cf49d8";
    DecimalFormat df = new DecimalFormat("#.##");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        City = findViewById(R.id.City);
        Result = findViewById(R.id.Result);
    }

    public void getWeatherDetails(View view) {
        String tosend = City.getText().toString();
        if (tosend.equals(""))
        {
            Result.setText("City Field Cannot Be Empty");
        }
        else
        {
            Intent intent = new Intent(MainActivity.this, Status.class);
            intent.putExtra("message_key", tosend);
            startActivity(intent);
        }

    }
}