package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Status extends AppCompatActivity {
    EditText City;
    TextView Result;
    private final String url = "http://api.weatherstack.com/current";
    private final String key = "ff0fd078fd5ffa42ee7c523a71cf49d8";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        TextView finalText = findViewById(R.id.ResultFinal);

        Intent intent = getIntent();
        String City = intent.getStringExtra("message_key");
        String final_url = url + "?access_key=" + key + "&query=" + City;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, final_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONObject request = response.getJSONObject("request");
                    String description = request.getString("query");
                    JSONObject current = response.getJSONObject("current");
                    String time = current.getString("observation_time");
                    String temperature = current.getString("temperature");
                    String windspeed = current.getString("wind_speed");
                    String winddir = current.getString("wind_dir");
                    String precip = current.getString("precip");
                    String humidity = current.getString("humidity");
                    JSONArray jsonWeather = current.getJSONArray("weather_descriptions");
                    String Overall = jsonWeather.get(0).toString();

                    String Output = "Location:\t" + description + "\n\nTime:\t" + time
                    + "\n\nTemperature:\t" + temperature + "\n\nOverall:\t" + Overall + "\n\nWindSpeed:\t"
                            + windspeed + "\n\nWind Direction:\t" + winddir + "\n\nPrecip:\t" + precip
                            + "\n\nHumidity:\t" + humidity + "\n";

                    finalText.setText(Output);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT);
                toast.show();
                error.printStackTrace();
            }
        });
        requestQueue.add(req);
    }

}